APP_NAME   := bugs
JOB_NAME   := ${APP_NAME}-job
GIT_COMMIT := $(shell git rev-parse --short HEAD)
DOCKER_REGISTRY := ""
APP_IMAGE  := ${DOCKER_REGISTRY}${APP_NAME}
JOB_IMAGE  := ${DOCKER_REGISTRY}${JOB_NAME}

all: docker-build

.PHONY: docker-build
docker-build:
	docker build -t ${APP_IMAGE}:latest -f Dockerfile .
	docker build -t ${JOB_IMAGE}:latest -f Dockerfile-job .

.PHONY: docker-push
docker-push:
	docker tag ${APP_IMAGE}:latest ${APP_IMAGE}:${GIT_COMMIT}
	docker push ${APP_IMAGE}:latest
	docker push ${APP_IMAGE}:${GIT_COMMIT}
	docker tag ${JOB_IMAGE}:latest ${JOB_IMAGE}:${GIT_COMMIT}
	docker push ${JOB_IMAGE}:latest
	docker push ${JOB_IMAGE}:${GIT_COMMIT}

.PHONY: deploy
deploy:
	cat deploy/kubernetes.yaml | GIT_COMMIT=${GIT_COMMIT} envsubst | kubectl apply -f -
