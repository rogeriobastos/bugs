# bugs

![bugs](/doc/banner.jpg)

Bugs is a dashboard for vulnerabilities alerts from Github Dependabot and an ally of Neo[^1].

[^1]: https://matrix.fandom.com/wiki/Bugs
