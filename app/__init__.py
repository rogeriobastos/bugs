from flask import Flask
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from os import getenv


db = SQLAlchemy()
bcrypt = Bcrypt()


def create_app():
    app = Flask(__name__)

    from .config import config
    FlaskConfig = config[getenv('FLASK_ENV', 'default')]
    app.config.from_object(FlaskConfig())

    db.init_app(app)
    bcrypt.init_app(app)

    login_manager = LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = "view.login"

    from .model import User

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(user_id)

    from .view import bp
    app.register_blueprint(bp)

    return app
