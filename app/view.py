from sqlalchemy.orm import joinedload
from flask import Blueprint, redirect, render_template, session, url_for
from flask_login import login_required, login_user, logout_user
from .form import LoginForm
from .model import Repository, Sync, StatsOrganizationDaily, StatsRepositoryDaily, User, Vulnerability


bp = Blueprint('view', __name__)


@bp.route('/healthz')
def health():
    return 'OK'


# TODO: cookie doesn't expire?
@bp.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.username.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user)
            if 'next' in session:
                next_url = session['next']
            else:
                next_url = url_for('view.home')
            return redirect(next_url)
    return render_template('login.html', form=form)


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('view.login'))


@bp.route('/')
@login_required
def home():
    stats = {}
    stats['repos-count'] = Repository.query.filter_by(is_archived=False).count()
    stats['vulns-count'] = Vulnerability.query.filter_by(dismissed_at=None).count()
    vulns = Vulnerability.query.options(joinedload('repository')).filter_by(dismissed_at=None).order_by(Vulnerability.created_at.desc()).limit(30).all()

    stats['pie'] = [0,0,0,0]
    stats['stacked'] = {
            'labels': [],
            'critical': [],
            'high': [],
            'moderate': [],
            'low': []
    }
    
    s = StatsOrganizationDaily.query.order_by(StatsOrganizationDaily.created_at.desc()).limit(6).all()
    if s:
        stats['pie'][0] = s[0].total_critical
        stats['pie'][1] = s[0].total_high
        stats['pie'][2] = s[0].total_moderate
        stats['pie'][3] = s[0].total_low

        for i in reversed(s):
            stats['stacked']['labels'].append(str(i.created_at))
            stats['stacked']['critical'].append(i.total_critical)
            stats['stacked']['high'].append(i.total_high)
            stats['stacked']['moderate'].append(i.total_moderate)
            stats['stacked']['low'].append(i.total_low)

    return render_template('home.html', vulns=vulns, stats=stats)


@bp.route('/repos')
@login_required
def repos_list():
    repos = []
    last_sync = Sync.query.filter_by(name='stats_repos').first()
    if last_sync:
        repos = StatsRepositoryDaily.query.filter_by(created_at=last_sync.date).all()
    return render_template('repos.html', repos=repos)


@bp.route('/repos/<name>')
@login_required
def repo_show(name):
    stats = {}
    repo = Repository.query.filter_by(name=name).first_or_404()
    vulns = Vulnerability.query.with_parent(repo).filter_by(dismissed_at=None).all()

    stats['pie'] = [0,0,0,0]
    stats['stacked'] = {
            'labels': [],
            'critical': [],
            'high': [],
            'moderate': [],
            'low': []
    }
    
    s = StatsRepositoryDaily.query.order_by(StatsRepositoryDaily.created_at.desc()).filter_by(repository_name=repo.name).limit(6).all()
    if s:
        stats['pie'][0] = s[0].total_critical
        stats['pie'][1] = s[0].total_high
        stats['pie'][2] = s[0].total_moderate
        stats['pie'][3] = s[0].total_low

        for i in reversed(s):
            stats['stacked']['labels'].append(str(i.created_at))
            stats['stacked']['critical'].append(i.total_critical)
            stats['stacked']['high'].append(i.total_high)
            stats['stacked']['moderate'].append(i.total_moderate)
            stats['stacked']['low'].append(i.total_low)

    # TODO: sort vulns by severity in template
    # use this https://datatables.net/manual/data/orthogonal-data
    return render_template('repo_show.html', repo=repo, vulns=vulns, stats=stats)


@bp.route('/vulns')
@login_required
def vulns_list():
    vulns = Vulnerability.query.options(joinedload('repository')).filter_by(dismissed_at=None).all()
    return render_template('vulns.html', vulns=vulns)

