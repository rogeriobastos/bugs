from flask_login import UserMixin
from sqlalchemy.ext.hybrid import hybrid_property

from . import bcrypt, db


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True, nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)

    @hybrid_property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, password):
        self.password_hash = bcrypt.generate_password_hash(password).decode('utf-8')

    def verify_password(self, password):
        return bcrypt.check_password_hash(self.password_hash, password)


class Sync(db.Model):
    name = db.Column(db.String, primary_key=True)
    date = db.Column(db.Date)


class Repository(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True)
    is_archived = db.Column(db.Boolean)
    link = db.Column(db.String)


class Vulnerability(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ghsa_id = db.Column(db.String)
    repository_id = db.Column(db.Integer, db.ForeignKey('repository.id'), nullable=False)
    repository = db.relationship('Repository', backref=db.backref('vulnerabilities', lazy=True))
    severity = db.Column(db.String)
    pkg_system = db.Column(db.String)
    pkg_name = db.Column(db.String)
    link = db.Column(db.String)
    summary = db.Column(db.String)
    score = db.Column(db.Float)
    created_at = db.Column(db.DateTime)
    dismissed_at = db.Column(db.DateTime)


class StatsRepositoryDaily(db.Model):
    created_at = db.Column(db.Date, primary_key=True)
    repository_name = db.Column(db.String, primary_key=True, nullable=False)
    total_critical = db.Column(db.Integer, default=0)
    total_high = db.Column(db.Integer, default=0)
    total_moderate = db.Column(db.Integer, default=0)
    total_low = db.Column(db.Integer, default=0)


class StatsOrganizationDaily(db.Model):
    created_at = db.Column(db.Date, primary_key=True)
    total_critical = db.Column(db.Integer, default=0)
    total_high = db.Column(db.Integer, default=0)
    total_moderate = db.Column(db.Integer, default=0)
    total_low = db.Column(db.Integer, default=0)

