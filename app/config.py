from os import getenv


class Config:
    SECRET_KEY = 'e97ab6287a5ab246a6e45502517d578ee685838ab0763d49f49adb14ded4ce3c'
    REMEMBER_COOKIE_DURATION = 86400 # 1 day
    USE_SESSION_FOR_NEXT = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class DevelopmentConfig(Config):
    DEBUG = True
    ENV = 'development'
    GITHUB_TOKEN = getenv('GITHUB_TOKEN')
    SQLALCHEMY_DATABASE_URI = getenv('DATABASE_URI')
    SQLALCHEMY_ECHO = True

class ProductionConfig(Config):
    def load_secret(self, path):
        secret = None
        try:
            with open(path, 'r') as f:
                secret = f.read()
        except:
            pass
        return secret

    @property
    def SECRET_KEY(self):
        secret_key = self.load_secret('/app/secret/flask_secret_key')

        if not secret_key:
            raise ValueError("No SECRET_KEY set for Flask application")
        return secret_key

    @property
    def SQLALCHEMY_DATABASE_URI(self):
        db_uri = self.load_secret('/app/secret/database_uri')

        if not db_uri:
            raise ValueError("No DATABASE_URI set for Flask application")
        return db_uri


class JobConfig(ProductionConfig):
    GITHUB_TOKEN = getenv('GITHUB_TOKEN')


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'job': JobConfig,
    'default': DevelopmentConfig
}
