FROM alpine:3.15 AS builder
RUN apk add --no-cache gcc g++ musl-dev python3 python3-dev libffi-dev libpq-dev
WORKDIR /app
COPY requirements.txt ./
# Setup the virtualenv
RUN python3 -m venv /venv \
 && source /venv/bin/activate \
 && pip install --no-cache-dir -r requirements.txt


FROM alpine:3.15
RUN apk add --no-cache python3 uwsgi uwsgi-python3 libffi libpq
COPY --from=builder /venv /venv
# Force the stdout and stderr streams to be unbuffered
ENV PYTHONUNBUFFERED=1
RUN addgroup --system app \
    && adduser --system --ingroup app --no-create-home app
USER app
WORKDIR /app
COPY . ./
CMD ["uwsgi", "/app/config/uwsgi.ini"]
