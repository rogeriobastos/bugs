import click
from datetime import datetime, timedelta, timezone
from flask.cli import AppGroup
from gql import gql, Client
from gql.transport.aiohttp import AIOHTTPTransport
from sqlalchemy import func
from secrets import token_hex

from app import create_app, db
from app.model import Repository, StatsOrganizationDaily, StatsRepositoryDaily, Sync, User, Vulnerability

app = create_app()


db_cli = AppGroup('db', help='Database commands.')

@db_cli.command('init', help='Initialize database.')
def db_init():
    db.create_all()

app.cli.add_command(db_cli)


user_cli = AppGroup('user', help='User management commands.')

@user_cli.command('list', help='List all users.')
def user_list():
    users = User.query.all()
    for user in users:
        print(user.email)

@user_cli.command('create', help='Create an user.')
@click.argument('email')
def user_create(email):
    user = User.query.filter_by(email=email).first()
    if user:
        print(f'User {email} already exist.')
    else:
        p = token_hex(20)
        u = User(email=email, password=p)
        db.session.add(u)
        db.session.commit()
        print(f'User {email} created with password: {p}')

@user_cli.command('delete', help='Delete an user.')
@click.argument('email')
def user_delete(email):
    user = User.query.filter_by(email=email).first()
    if user:
        if click.confirm(f'Do you really want to delete {email}?'):
            db.session.delete(user)
            db.session.commit()
            print(f'User {email} deleted.')
        else:
            print('Aborting.')
    else:
        print(f'User {email} doesn\'t exist.')

app.cli.add_command(user_cli)


vulns_cli = AppGroup('vulns', help='Vulnerabilities data commands.')

def today():
    tz = timezone(offset=-timedelta(hours=3))
    return datetime.now(tz=tz).date()

def vulns_db_clear():
    # cleanning vulnerability table
    try:
        Vulnerability.query.delete()
        db.session.commit()
    except:
        db.session.rollback()
        raise Exception("Can't clean up vulnerability table")
    
    # cleanning repository table
    try:
        Repository.query.delete()
        db.session.commit()
    except:
        db.session.rollback()
        raise Exception("Can't clean up repository table")

def graphql_init():
    # Select your transport with a defined url endpoint
    transport = AIOHTTPTransport(
        url='https://api.github.com/graphql',
        headers={
            'Authorization': f"bearer {app.config['GITHUB_TOKEN']}",
            'Content-type': 'application/json',
        }
    )

    # Create a GraphQL client using the defined transport
    return Client(transport=transport)

def vulns_sync_repos(gql_client):
    query = gql("""
        query getRepositories ($endCursor: String) {
          organization(login: "org-name") {
            repositories(first: 100 after: $endCursor) {
              nodes {
                name
                isArchived
                isPrivate
                url
              }
              pageInfo {
                endCursor
                hasNextPage
              }
            }
          }
        }
    """
    )
    
    params = {'endCursor': None}
    
    while True:
        result = gql_client.execute(query, variable_values=params)
        for node in result['organization']['repositories']['nodes']:
            print(f'Adding repository {node["name"]}')
            db.session.add(
                Repository(name=node['name'], is_archived=node['isArchived'], link=node['url'])
            )
        db.session.commit()
    
        if result['organization']['repositories']['pageInfo']['hasNextPage']:
            params['endCursor'] = result['organization']['repositories']['pageInfo']['endCursor']
        else:
            break

    # Update sync table
    db.session.merge(
            Sync(name='repository', date=today())
    )
    db.session.commit()


def datetime_parser(dt):
    if dt is None:
        return None
    return datetime.fromisoformat(dt[:-1])


def vulns_sync_vulns(gql_client):
    query = gql("""
        query getVulnerabilities ($repository: String!, $endCursor: String) {
          repository(name: $repository, owner: "org-name") {
            vulnerabilityAlerts(first: 100 after: $endCursor) {
              pageInfo {
                endCursor
                hasNextPage
              }
              nodes {
                securityVulnerability {
                  severity
                  package {
                    ecosystem
                    name
                  }
                  advisory {
                    ghsaId
                    permalink
                    summary
                    cvss {
                      score
                    }
                  }
                }
                dismissedAt
                createdAt
              }
            }
          }
        }
    """
    )
    
    
    repos = Repository.query.with_entities(Repository.id, Repository.name).filter_by(is_archived=False).all()
    for repo in repos:
        params = {
                'repository': repo[1],
                'endCursor': None
        }
        while True:
            result = gql_client.execute(query, variable_values=params)
            for node in result['repository']['vulnerabilityAlerts']['nodes']:
                print(f"Adding vulnerability {node['securityVulnerability']['advisory']['ghsaId']} in repository {repo[1]}")
                db.session.add(
                        Vulnerability(
                            ghsa_id=node['securityVulnerability']['advisory']['ghsaId'],
                            created_at = datetime_parser(node['createdAt']),
                            dismissed_at = datetime_parser(node['dismissedAt']),
                            repository_id=repo[0],
                            severity=node['securityVulnerability']['severity'],
                            pkg_system=node['securityVulnerability']['package']['ecosystem'],
                            pkg_name=node['securityVulnerability']['package']['name'],
                            link=node['securityVulnerability']['advisory']['permalink'],
                            summary=node['securityVulnerability']['advisory']['summary'],
                            score=node['securityVulnerability']['advisory']['cvss']['score']
                        )
                )
            db.session.commit()
            if result['repository']['vulnerabilityAlerts']['pageInfo']['hasNextPage']:
                params['endCursor'] = result['repository']['vulnerabilityAlerts']['pageInfo']['endCursor']
            else:
                break

    # Update sync table
    db.session.merge(
            Sync(name='vulnerability', date=today())
    )
    db.session.commit()


@vulns_cli.command('update', help='Download Github\'s data.')
def vulns_update():
    vulns_db_clear()
    gql_client = graphql_init()
    vulns_sync_repos(gql_client)
    vulns_sync_vulns(gql_client)


def vulns_stats_org():
    last_sync = Sync.query.filter_by(name='vulnerability').first()
    if not last_sync:
        return

    sync_date = last_sync.date
    vulns = Vulnerability.query.with_entities(Vulnerability.severity, func.count(Vulnerability.id)).filter(Vulnerability.dismissed_at==None).group_by(Vulnerability.severity).all()
    v = { 'c': 0, 'h': 0, 'm': 0, 'l': 0 }
    for i in vulns:
        if i[0] == 'CRITICAL': v['c'] = i[1]
        elif i[0] == 'HIGH': v['h'] = i[1]
        elif i[0] == 'MODERATE': v['m'] = i[1]
        elif i[0] == 'LOW': v['l'] = i[1]
    db.session.merge(
        StatsOrganizationDaily(
            created_at=sync_date,
            total_critical=v['c'],
            total_high=v['h'],
            total_moderate=v['m'],
            total_low=v['l']
        )
    )
    db.session.commit()

    # Update sync table
    db.session.merge(
            Sync(name='stats_org', date=sync_date)
    )
    db.session.commit()


def vulns_stats_repos():
    last_sync = Sync.query.filter_by(name='repository').first()
    if not last_sync:
        return

    sync_date = last_sync.date
    for repo in Repository.query.filter_by(is_archived=False).all():
        vulns = Vulnerability.query.with_entities(Vulnerability.severity, func.count(Vulnerability.id)).join(Repository).filter(Repository.id==repo.id).filter(Vulnerability.dismissed_at==None).group_by(Vulnerability.severity).all()
        v = { 'c': 0, 'h': 0, 'm': 0, 'l': 0 }
        for i in vulns:
            if i[0] == 'CRITICAL': v['c'] = i[1]
            elif i[0] == 'HIGH': v['h'] = i[1]
            elif i[0] == 'MODERATE': v['m'] = i[1]
            elif i[0] == 'LOW': v['l'] = i[1]
        db.session.merge(
            StatsRepositoryDaily(
                created_at=sync_date,
                repository_name=repo.name,
                total_critical=v['c'],
                total_high=v['h'],
                total_moderate=v['m'],
                total_low=v['l']
            )
        )
    db.session.commit()

    # Update sync table
    db.session.merge(
            Sync(name='stats_repos', date=sync_date)
    )
    db.session.commit()


@vulns_cli.command('stats', help='Compute statistics about vulnerabilities.')
def vulns_stats_compute():
    print('Starting stats task.')
    print('Computing org stats.')
    vulns_stats_org()
    print('Computing repos stats.')
    vulns_stats_repos()
    print('Finnishing stats task.')


app.cli.add_command(vulns_cli)
